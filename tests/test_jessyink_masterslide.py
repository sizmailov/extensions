# coding=utf-8
from jessyInk_masterSlide import MasterSlide
from inkex.tester import ComparisonMixin, TestCase

class JessyInkMasterSlideBasicTest(ComparisonMixin, TestCase):
    effect_class = MasterSlide
